// git-ignore is a command line program that either lists or adds to a
// .gitignore file.
package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strings"

	"md0.org/fileutil"
)

const VERSION = "0.0.1"

func main() {
	var (
		v         = flag.Bool("v", false, "print version")
		gitignore = "./.gitignore"
	)

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "git-ignore: list or add to .gitignore file.\n")
		fmt.Fprintf(os.Stderr, "\n")
		fmt.Fprintf(os.Stderr, "usage:\n")
		fmt.Fprintf(os.Stderr, "  git-ignore      // list .gitignore file\n")
		fmt.Fprintf(os.Stderr, "  git-ignore line // add line to .gitignore\n")
		fmt.Fprintf(os.Stderr, "\n")
		fmt.Fprintf(os.Stderr, "  Add multiple lines with space separated list.\n")
		fmt.Fprintf(os.Stderr, "  Quote lines if they contain spaces.\n")
		fmt.Fprintf(os.Stderr, "  If git-ignore is in your PATH, invoke it as 'git ignore'.\n")
		fmt.Fprintf(os.Stderr, "  You must be in your repository root for git-ignore to work.\n")
		fmt.Fprintf(os.Stderr, "\n")
		flag.PrintDefaults()
		os.Exit(1)
	}
	flag.Parse()

	if *v {
		fmt.Println(VERSION)
		return
	}

	if !fileutil.IsFile(gitignore) {
		fmt.Fprintf(os.Stderr, "no file: %s\n", gitignore)
		flag.Usage()
	}

	switch flag.NArg() {
	case 0:
		if err := catFile(os.Stdout, gitignore); err != nil {
			log.Fatal(err)
		}
	default:
		if err := append(gitignore, flag.Args()); err != nil {
			log.Fatal(err)
		}
	}
}

// Stolen from u-root project, cmds/cat/cat.go
func catFile(w io.Writer, file string) error {
	f, err := os.Open(file)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = io.Copy(w, f)
	return err
}

func append(file string, lines []string) error {
	f, err := os.OpenFile(file, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = f.WriteString(strings.Join(lines, "\n") + "\n")
	return err
}
