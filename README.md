# git-ignore

> **(May 2021)** moved to [md0.org/git-ignore](https://md0.org/git-ignore).

git-ignore is a command line program that lists or adds to a .gitignore
file.

If the git-ignore executable is in you PATH, you can invoke it as `git
ignore`.

## Usage:

list .gitignore, like `cat .gitignore`:

```
git ignore
```

add a line (or lines) to .gitignore:

```
git ignore line1 [line2 ...]
```

## Good-to-knows:

The .gitignore file must be in the current directory (that is, you have
to be in the root of your repository).

You can add multiple lines at a time, just list them one after another.
If a line contains a space, quote the whole line.
